"""
Build a dataset from a set of images
Dataset from: https://github.com/byeongkeun-kang/HOI-dataset
"""
import logging
import os

import cv2
from ovnImage import check_dir

from hands_rdf.Model import config
from kinect_dataset_builder.kinect_data_builder.ImageProcess import Frames

log = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%I:%M:%S', level=logging.DEBUG)

# PATH to store the result images
PATH_DATASET = config.FOLDER_DATA + "/datasets/HOI_processed/"
check_dir(PATH_DATASET)

# PATH of the frames
folder_ds = config.FOLDER_DATA + "/datasets/HOI/HOI_training_data/training_data"

frames = Frames(
    folder_ds + "/depth/",
    path_ground_truth=folder_ds + "/label/",
)

"""
    MAIN LOGIC
"""


frames.__iter__()
while frames.current < frames.last:
    frames.__next__()
    print("current image: ", os.path.basename(frames.d_files[frames.current]))
    im_save = frames.get_im_format_save()
    cv2.imwrite(PATH_DATASET + str(frames.current) + ".png", im_save)

"""
Build a dataset from a set of images
"""
import os
import logging
import cv2
from hands_rdf.Model.Config import config

from kinect_dataset_builder.kinect_data_builder.ImageProcess import Frames
from kinect_dataset_builder.kinect_data_builder.gui_hsv_segmentation import GUI

from ovnImage import check_dir

log = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%I:%M:%S', level=logging.DEBUG)

# PATH to store the result images
PATH_DATASET = config.FOLDER_DATA + "/BG_dataset/depth/"
PATH_SAVE_COLOR = config.FOLDER_DATA + "/BG_dataset/color/"
check_dir(PATH_DATASET)
check_dir(PATH_SAVE_COLOR)


# PATH of the frames
PATH_FRAMES = config.FOLDER_DATA + "/frames/BG/"

frames = Frames(
    PATH_FRAMES + "depth/",
    PATH_FRAMES + "color/"
)
frames.set_depth_processing()


"""
    MAIN LOGIC
"""
gui = GUI(frames, PATH_DATASET)


def after_run(gui):
    """

    :param gui: GUI
    :return:
    """
    im_path = gui.frames.c_files[gui.frames.current]
    im_bgra = cv2.imread(im_path, -1)
    cv2.imwrite(PATH_SAVE_COLOR + os.path.basename(im_path), im_bgra)


gui.func_after_write.append(after_run)

gui.start()

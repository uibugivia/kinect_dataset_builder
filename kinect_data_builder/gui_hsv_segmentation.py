import cv2
import numpy as np
from matplotlib.widgets import Button, Slider

from ovnImage import InteractivePlot
from kinect_dataset_builder.kinect_data_builder.utils import generate_filename


class GUI:
    def __init__(self, frames, path_2_save):

        self.block = False
        self.frames = frames

        self.plotter = InteractivePlot()
        self.path_save = path_2_save

        self.func_after_write = []

        axcolor = 'lightgoldenrodyellow'
        self.ax_low_hue = self.plotter.fig.add_axes([0.25, 0.06, 0.25, 0.01], facecolor=axcolor)
        self.ax_upper_hue = self.plotter.fig.add_axes([0.6, 0.06, 0.25, 0.01], facecolor=axcolor)

        self.ax_low_sat = self.plotter.fig.add_axes([0.25, 0.08, 0.25, 0.01], facecolor=axcolor)
        self.ax_upper_sat = self.plotter.fig.add_axes([0.6, 0.08, 0.25, 0.01], facecolor=axcolor)

        self.ax_low_value = self.plotter.fig.add_axes([0.25, 0.1, 0.25, 0.01], facecolor=axcolor)
        self.ax_upper_value = self.plotter.fig.add_axes([0.6, 0.1, 0.25, 0.01], facecolor=axcolor)

        self.slider_axes = [
            self.ax_low_hue, self.ax_low_sat, self.ax_low_value, self.ax_upper_hue,
            self.ax_upper_sat, self.ax_upper_value
        ]
        self.axprev = self.plotter.fig.add_axes([0.01, 0.05, 0.1, 0.05])
        self.axnext = self.plotter.fig.add_axes([0.89, 0.05, 0.1, 0.05])

        frames.th_ch0 = np.array([170, 190])
        frames.th_ch1 = np.array([150, 255])
        frames.th_ch2 = np.array([50, 255])

    def start(self, initial_frame=0):

        self.frames.__iter__()
        self.frames.current = initial_frame
        images = self.frames.__next__()
        while self.frames.current < self.frames.last:
            print("image: ", self.frames.current)
            self.update()
            if not self.block:
                self.block = True
                images = self.frames.__next__()

    def update(self):
        self.plotter.attach_key_press_event(self.on_key)

        bnext = Button(self.axnext, 'Next')
        # bnext.on_clicked(callback.next)
        bprev = Button(self.axprev, 'Previous')
        # bprev.on_clicked(callback.prev)
        # set useblit = True on gtkagg for enhanced performance
        # cursor = Cursor(plotter.unused_axes[0], useblit=True, color='red', linewidth=2)
        for sl_ax in self.slider_axes:
            sl_ax.clear()

        s_l_h = Slider(self.ax_low_hue, 'lower hue', 0, 255, valinit=self.frames.th_ch0[0], valfmt="%i")
        s_l_s = Slider(self.ax_low_sat, 'lower saturation', 0, 255, valinit=self.frames.th_ch1[0], valfmt="%i")
        s_l_v = Slider(self.ax_low_value, 'lower value', 0, 255, valinit=self.frames.th_ch2[0], valfmt="%i")

        s_u_h = Slider(self.ax_upper_hue, 'Upper hue', 0, 255, valinit=self.frames.th_ch0[1], valfmt="%i")
        s_u_s = Slider(self.ax_upper_sat, 'Upper saturation', 0, 255, valinit=self.frames.th_ch1[1], valfmt="%i")
        s_u_v = Slider(self.ax_upper_value, 'Upper value', 0, 255, valinit=self.frames.th_ch2[1], valfmt="%i")

        s_l_h.on_changed(self.update_lower_hue)
        s_l_s.on_changed(self.update_lower_sat)
        s_l_v.on_changed(self.update_lower_value)
        s_u_h.on_changed(self.update_upper_hue)
        s_u_s.on_changed(self.update_upper_sat)
        s_u_v.on_changed(self.update_upper_value)

        print("""
        ----------CREATING SAMPLE----------
        press "q" key to discard the image
        press "w" key to write the image to disk
        -----------------------------------
        """)
        self.plotter.multi(self.frames.get_images(),
                     cmap='bwr')

    """
        EVENTS FUNCTIONS
    """

    def on_key(self, event):
        print('you pressed', event.key, event.xdata, event.ydata)
        if event.key == 'q':  # discard image
            self.block = False

        elif event.key == 'w':  # save image
            im_save = self.frames.get_im_format_save()
            cv2.imwrite(self.path_save + generate_filename(self.frames.current) + ".png", im_save)
            for f in self.func_after_write:
                f(self)

            self.block = False

    def update_lower_hue(self, val):
        self.frames.th_ch0[0] = val
        self.plotter.update(self.frames.get_images(),
                       cmap='bwr')

    def update_upper_hue(self, val):
        self.frames.th_ch0[1] = val
        self.plotter.update(self.frames.get_images(),
                       cmap='bwr')

    def update_lower_sat(self, val):
        self.frames.th_ch1[0] = val
        self.plotter.update(self.frames.get_images(),
                       cmap='bwr')

    def update_upper_sat(self, val):
        self.frames.th_ch1[1] = val
        self.plotter.update(self.frames.get_images(),
                       cmap='bwr')

    def update_lower_value(self, val):
        self.frames.th_ch2[0] = val
        self.plotter.update(self.frames.get_images(),
                       cmap='bwr')

    def update_upper_value(self, val):
        self.frames.th_ch2[1] = val
        self.plotter.update(self.frames.get_images(),
                       cmap='bwr')


def generate_filename(n_frame, length=7):
    name = str(n_frame)
    return '0'*(length - len(name)) + name

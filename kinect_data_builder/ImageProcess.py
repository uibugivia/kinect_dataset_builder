import cv2
import numpy as np
from glob import glob


def masks_remove_small_contours(mask, min_size=100):
    contours = []
    _, all_contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    for cnt in all_contours:
        area = cv2.contourArea(cnt)
        if area > min_size:
            contours.append(cnt)
    res = np.zeros(mask.shape, dtype=mask.dtype)
    cv2.drawContours(res, contours, -1, 255, -1)
    return res


def retrieve_hand_mask_hsv(im_color, th_hue, th_sat, th_val):
    hand_mask = cv2.inRange(im_color, np.array([th_hue[0], th_sat[0], th_val[0]]),
                            np.array([th_hue[1], th_sat[1], th_val[1]]))
    hand_mask = hand_mask.astype(np.uint8)
    hand_mask = masks_remove_small_contours(hand_mask, 100)

    return hand_mask


class Frames:
    """
    Class to iterate over image files.

    The images used must be structured in three different folders:
    - First, containing depth images.
    - Second, containing color images.
    - Third, with binary masks.
    Each folder must have the equivalent frames in the other folders with the same name.


    Parameters
    ---------

    """

    def __init__(self, path_depth, path_color=None, path_ground_truth=None):
        """

        :param path_depth: String Path of the depth images
        :param path_color: String|None
        :param path_ground_truth: String|None
        """
        self.d_files = sorted(glob(path_depth + "*.png"))
        self.c_files = None if path_color is None else sorted(glob(path_color + "*.png"))
        self.gt_files = None if path_ground_truth is None else sorted(glob(path_ground_truth + "*.png"))

        n_items = [len(f_list) for f_list in [self.d_files, self.c_files, self.gt_files] if f_list is not None]
        self.last = np.min([n_items])

        self.process_depth = False
        self.th_ch0 = np.array([0, 255])
        self.th_ch1 = np.array([0, 255])
        self.th_ch2 = np.array([0, 255])

    def __iter__(self):
        """
        Init the iterator.

        :return:
        """
        self.current = 0
        return self

    def __next__(self):
        """
        Iterate over the object. Get next Frame.

        :return: string, BaseModel
        """
        if self.current <= self.last:
            # read color
            if self.c_files is not None:  # if we have color files
                im_bgra = cv2.imread(self.c_files[self.current], -1)
                self.im_background = im_bgra[:, :, 3]
                self.im_color = cv2.cvtColor(im_bgra, cv2.COLOR_BGRA2RGB)
                self.im_rgb = self.im_color.copy()

                self.im_color = cv2.cvtColor(self.im_color, cv2.COLOR_RGB2HSV)
                hue = self.im_color[:, :, 0]
                hue[hue < 75] += 180
                self.im_color[:, :, 0][self.im_background == 0] = 0

            # read depth
            self.im_depth_original = cv2.imread(self.d_files[self.current], -cv2.IMREAD_ANYDEPTH)
            self.im_depth = self.processed_depth()

            # read ground truth
            if self.gt_files is not None:
                self.__hand_mask = cv2.imread(self.gt_files[self.current], cv2.IMREAD_GRAYSCALE) * 255

            images = self.get_images()
            self.current += 1
            if images is None:
                return self.__next__()

            return images
        else:
            raise StopIteration

    def set_depth_processing(self, value=True):
        """
        Indicate if depth has to be processed.

        :param value: bool
        :return:
        """
        self.process_depth = value

    @property
    def item_mask(self):
        """
        Get a mask thresholding with the configured class thresholds. (th_ch0, th_ch1, th_ch2)
        If the Ground Truth path is configured return the current Frame image.

        :return:
        binary mask
        """
        if self.gt_files is None:

            return retrieve_hand_mask_hsv(self.im_color, self.th_ch0, self.th_ch1, self.th_ch2)
        else:
            return self.__hand_mask

    def processed_depth(self):
        """
        Get the current depth frame processed to discard his background.

        Set to the max value all background and null pixels.


        :return:
        uint16 matrix, the current depth frame processed.
        """
        if not self.process_depth:
            return self.im_depth_original.copy()

        if np.max(self.im_background) == 1:
            bg_msk = np.invert(self.im_background * 255)
        else:
            bg_msk = np.invert(self.im_background)

        kernel = np.ones((3, 3), np.float32) / 10
        # TODO: filter before get biggest connected component
        bg_msk = cv2.filter2D(bg_msk.astype(np.uint8), -1, kernel).astype(np.bool)

        im_depth = self.im_depth_original.copy()
        im_depth[bg_msk] = np.iinfo(self.im_depth_original.dtype).max
        im_depth[im_depth == 0] = np.iinfo(self.im_depth_original.dtype).max
        return im_depth

    def get_images(self):
        """
        Get the interesting images to plot of the current frame.

        :return:
        dict
        """
        images = [
            {
                "img": self.get_im_format_save(),
                "title": "im_save"
            },
            {
                "img": self.item_mask,
                "title": "hand mask"
            },
            {
                "img": self.im_depth_original,
                "title": "depth",
                'colorbar': True
            }
        ]

        if self.c_files is not None:
            hue = self.im_color[:, :, 0]

            images.append({
                "img": hue,
                "title": "color"
            })
            images.append({
                "img": self.im_rgb,
                "title": "color rgb"
            })

        return images

    def get_im_format_save(self):
        """
        Get an image with a correct format to save all the current frame information in disk.
        :return:
        uint8 3 chanel image
        """
        im_save = np.zeros((self.im_depth.shape[0], self.im_depth.shape[1], 3), dtype=np.uint8)
        hand_mask = cv2.resize(self.item_mask, (self.im_depth.shape[1], self.im_depth.shape[0]))
        im_save[:, :, 0] = self.im_depth & 0x00FF
        im_save[:, :, 1] = self.im_depth >> 8
        im_save[:, :, 2] = hand_mask

        return im_save

# kinect_dataset_builder

Utilities to create a dataset from a frames obtained with a kinect device.

Semiautomatic sequential image segmentation using color information.

# Classes

## GUI
Create the GUI used to create the dataset.

## Frames
Object used to manage the collection of images to process.

# Examples

In the folder examples there are some examples of how to use the library.